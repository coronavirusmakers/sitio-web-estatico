# CVM Static Web

Publish the pages of wordpress "www.coronavirusmakers.org" into gitlab pages to preserve contents over the time.

The files of "seed" folder will be copied into "public" folder to be published using gitlab pages. This static files was scrapped from the original wordpress site on 2020-12-07 with using `wget` like this:

```console
wget -mkxKE -e robots=off  https://www.coronavirusmakers.org
```

Review the `.gitlab-ci.yml` pipeline file and change the site URL vairable on line 2 to fit with the right DNS name. Follow the [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) guide to know how to point a DNS to the gitlab pages.
